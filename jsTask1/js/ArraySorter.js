"use strict";
/**
* Array sorting object's constructor
*/
function ArraySorter() {}
/**
* Sorts array with bubble sort
* @param {number[]} array - array
* @return {number[]} sorted array
*/
ArraySorter.bubbleSort = function(array) {
    if (!Array.isArray(array)) {
        return -1;
    }
    for (var i = 0; i < array.length; i++) {
        for (var j = i + 1; j < array.length; j++) {
            if (array[i] > array[j]) {
                var tmp = array[i];
                array[i] = array[j];
                array[j] = tmp;
            }
        }
    }
    return array;
}
/**
* Sorts array with selection sort
* @param {number[]} array - array
* @return {number[]} sorted array
*/
ArraySorter.selectionSort = function(array) {
    if (!Array.isArray(array)) {
        return -1;
    }
    for (var i = 0; i < array.length; i++) {
        var min = i;
        for (var j = i + 1; j < array.length; j++) {
            if (array[min] > array[j]) {
                min = j;
            }
        }
        var tmp = array[i];
        array[i] = array[min];
        array[min] = tmp;
    }
    return array;
}
/**
* Sorts array with insertion sort
* @param {number[]} array - array
* @return {number[]} sorted array
*/
ArraySorter.insertionSort = function(array) {
    if (!Array.isArray(array)) {
        return -1;
    }
    for (var i = 1; i < array.length; i++) {
        for (var j = i; j > 0 && array[j - 1] > array[j]; j--) {
            var tmp = array[j - 1];
            array[j - 1] = array[j];
            array[j] = tmp;
        }
    }
    return array;
}
/**
* Sorts array with quick sort
* @param {number[]} array - array
* @return {number[]} sorted array
*/
ArraySorter.quickSort = function(array) {
    if (!Array.isArray(array)) {
        return -1;
    }
    function swap(array, firstIndex, secondIndex){
        var tmp = array[firstIndex];
        array[firstIndex] = array[secondIndex];
        array[secondIndex] = tmp;
    }
    function partition(array, left, right) {
        var pivot = array[Math.floor((right + left) / 2)], i = left, j = right;
        while (i <= j) {
            while (array[i] < pivot) {
                i++;
            }
            while (array[j] > pivot) {
                j--;
            }
            if (i <= j) {
                swap(array, i, j);
                i++;
                j--;
            }
        }
        return i;
    }
    function sort(array, left, right) {
        var index;
        if (array.length > 1) {
            index = partition(array, left, right);
            if (left < index - 1) {
                sort(array, left, index - 1);
            }
            if (index < right) {
                sort(array, index, right);
            }
        }
        return array;
    }
    return sort(array, 0, array.length - 1);
}