"use strict";
/**
* Date Formatter's constructor
*/
function DateDisplayFormatter() {
    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    /*
    * Parses string according to format 'DDMMYYYY'
    * @param {String} date - date in format 'DDMMYYYY'
    * @param {boolean} displayMonthName - if true, function displays month's name, otherwise it displays month's index number
    * @return {String} date in format 'DD-MM-YYYY' or in format 'DD month's name YYYY'
    */
    function parseStringByDefault(date, displayMonthName) {
        var day = date.substring(0, 2),
            month = date.substring(2, 4),
            year = date.substring(4, 8);
        if (!displayMonthName) {
            return day + "-" + month + "-" + year;
        }
        return day + " " + monthNames[month - 1] + " " + year;
    }
    /*
    * Parses string according to regular expressions
    * @param {String} date - date
    * @param {boolean} displayMonthName - if true, function displays month's name, otherwise it displays month's index number
    * @param {String} regularIn - regular expression for parsing date
    * @param {String} regularOut - regular expression for constructing string
    * @return {String} date
    */
    this.parseStringWithParameters = function(date, displayMonthName, regularIn, regularOut) {
        if (regularIn === undefined && regularOut === undefined) {
            return parseStringByDefault(date, displayMonthName);
        }
        if (regularIn === undefined) {
            var day = date.substring(0, 2),
            month = date.substring(2, 4),
            year = date.substring(4, 8);
        } else {
        var yearIndex = regularIn.search(/YYYY/),
            monthIndex = regularIn.search(/MM/),
            dayIndex = regularIn.search(/DD/);
            year = date.substring(yearIndex, yearIndex + 4),
            month = date.substring(monthIndex, monthIndex + 2),
            day = date.substring(dayIndex, dayIndex + 2);
        }
        if (regularOut === undefined) {
            return parseStringByDefault(day + month + year, displayMonthName);
        }
        return regularOut.replace("YYYY", year).replace("MM", month).replace("DD", day);
    }
    /*
    * Parses milliseconds to date
    * @param {String} ms - date in milliseconds
    * @param {boolean} displayMonthName - if true, function displays month's name, otherwise it displays month's index number
    * @return {String} date in format 'DD-MM-YYYY' or in format 'DD month's name YYYY'
    */
    function parseNumberByDefault(ms, displayMonthName) {
        var date = new Date(parseInt(ms, 10)),
            year = date.getFullYear(),
            month = date.getMonth(),
            day = date.getDay();
        if (!displayMonthName) {
            return day + "-" + month + "-" + year;
        }
        return day + " " + monthNames[month - 1] + " " + year;
    }
    /*
    * Parses string according to regular expressions
    * @param {String} ms - date
    * @param {boolean} displayMonthName - if true, function displays month's name, otherwise it displays month's index number
    * @param {String} regularOut - regular expression for constructing string
    * @return {String} date
    */
    this.parseNumberWithParameters = function(ms, displayMonthName, regularOut) {
        if (regularOut === undefined) {
            return parseNumberByDefault(ms, displayMonthName);
        }
        var date = new Date(parseInt(ms, 10)),
            year = date.getFullYear(),
            month = date.getMonth(),
            day = date.getDay();
        return regularOut.replace("YYYY", year).replace("MM", month).replace("DD", day);
    }
}
/*
* Parses string or number according to regular expressions
* @param {String} inputType - let function know if it works with string or with milliseconds
* @param {String} date - date
* @param {boolean} displayMonthName - if true, function displays month's name, otherwise it displays month's index number
* @param {String} regularIn - regular expression for parsing date
* @param {String} regularOut - regular expression for constructing string
* @return {String} date
*/
DateDisplayFormatter.format = function(inputType, date, displayMonthName, regularIn, regularOut) {
    if (inputType.toLowerCase() === "ms") {
        return new DateDisplayFormatter().parseNumberWithParameters(date, displayMonthName, regularOut);
    } else if (inputType.toLowerCase() === "string") {
        return new DateDisplayFormatter().parseStringWithParameters(date, displayMonthName, regularIn, regularOut);
    } else return date;
}
