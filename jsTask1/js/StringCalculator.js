"use strict";
/**
* Constructor of object which performs calculator functions with numbers in a string representation
*/
function StringCalculator() {}
/**
* Performs the specified operation with two numbers in the string representation
* @param {String} operation - operation
* @param {String} a - first number
* @param {String} b - second number
* @return {number} operation's result
*/
StringCalculator.calc = function(operation, a, b) {
    var A = parseFloat(a, 10), B = parseFloat(b, 10);
    switch (operation) {
        case "+":
            return A + B;
        case "-":
            return A - B;
        case "*":
            return A * B;
        case "/":
            return A / B;
        default:
            return "Incorrect operation!";
    }
};