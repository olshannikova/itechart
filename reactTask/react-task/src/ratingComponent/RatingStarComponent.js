import React from 'react'
import PropTypes from 'prop-types'
import styles from './fonts/fontello-2c05905d/css/fontello.css'

class RatingStarComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: false,
            hover: false
        }
    }
    starOnClickHandler(e) {
        const value = e.target.value;
        this.setState({active: true});
        this.props.changeRating(value);
    }
    onMouseEnterHandler(e) {
        const value = e.target.dataset.value;
        this.setState({hover: true});
        this.props.changePossibleRating(value);
    }
    onMouseLeaveHandler(e) {
        this.setState({hover: false});
        this.props.changePossibleRating(0);
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.currentRating >= this.props.value) {
            this.setState({active: true});
        } else {
            this.setState({active: false});
        }

        if (nextProps.possibleRating >= this.props.value) {
            this.setState({hover: true});
        } else {
            this.setState({hover: false});
        }
    }
    render() {
        const value = this.props.value;
        const id = "rating-" + value;
        const radioKey = "radio-rating-" + value;
        const labelKey = "label-rating-" + value;
        const spanKey = "single-star-rating-" + value;
        return (
            <span key={spanKey}>
                <label 
                    key={labelKey} 
                    htmlFor={id} 
                    className={this.state.active || this.state.hover ? "icon-star" : "icon-star-empty"}
                    onMouseEnter={this.onMouseEnterHandler.bind(this)}
                    onMouseLeave={this.onMouseLeaveHandler.bind(this)}
                    data-value={value}>
                </label>
                <input 
                    key={radioKey} 
                    id={id} 
                    type="radio" 
                    name="rating" 
                    value={value} 
                    hidden 
                    onClick={this.starOnClickHandler.bind(this)}
                />
            </span>
        )
    }
}

RatingStarComponent.propTypes = {
    changeRating: PropTypes.func,
    changePossibleRating: PropTypes.func
}

export default RatingStarComponent;