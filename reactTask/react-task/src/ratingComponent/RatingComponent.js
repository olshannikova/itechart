import React from 'react'
import PropTypes from 'prop-types'
import RatingStarComponent from './RatingStarComponent.js'

class RatingComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rating: 0,
            possibleRating: 0
        }
    }
    changeRating(value) {
        this.setState({rating: value});
    }
    changePossibleRating(value) {
        this.setState({possibleRating: value});
    }
    render() {
        let stars = [];
        for (let i = 0; i < this.props.starsAmount; i += 1) {
            const key = "rating-star-component-" + (i + 1);
            stars.push(
                <RatingStarComponent 
                    key={key} 
                    value={i + 1} 
                    changeRating={this.changeRating.bind(this)} 
                    changePossibleRating={this.changePossibleRating.bind(this)} 
                    currentRating={this.state.rating} 
                    possibleRating={this.state.possibleRating} 
            />);
        }
        return (
            <div>{stars}</div>
        )
    }
}

RatingComponent.defaultProps = {
    starsAmount: 5
}

RatingComponent.propTypes = {
    starsAmount: PropTypes.number
}

export default RatingComponent;