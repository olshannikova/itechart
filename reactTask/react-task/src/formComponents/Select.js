import React from 'react';
import PropTypes from 'prop-types';

class Select extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.defaultValue || ""
        }
    }
    valueChangedHandler(e) {
        this.setState({value: e.target.value});
    }
    render() {
        return (
            <select 
                value={this.state.value}
                onChange={this.valueChangedHandler.bind(this)}
            >{this.props.children}</select>
        )
    }
}


export default Select;