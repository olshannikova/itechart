import React from 'react';
import PropTypes from 'prop-types';
import './css/Input.css';

class Input extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.value || ""
        };
    }
    valueChangedHandler(e) {
        const value = e.target.value;
        this.setState({value: value});
        if (this.props.getValue) {
            this.props.getValue(this.props.id, value);
        }
    }
    render() {
        return (
            <input 
                type={this.props.type}
                className="react__Input"
                id={this.props.id}
                accept={this.props.accept}
                onChange={this.valueChangedHandler.bind(this)}
                required={this.props.isRequired}
                onBlur={this.props.onBlur}
                value={this.state.value}
            />
        );
    }
}

Input.propTypes = {
    type: PropTypes.oneOf(["button", "checkbox", "file", "hidden", "image", "password", "radio", "reset", "submit", 
                        "text", "color", "date", "datetime", "datetime-local", "email", "number", "range", "search", 
                        "tel", "time", "url", "month", "week"]),
    id: PropTypes.any.isRequired,
    getValue: PropTypes.func,
    onBlur: PropTypes.func
};

Input.defaultProps = {
    type: "text"
};

export default Input;