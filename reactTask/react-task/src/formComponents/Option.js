import React from 'react';
import PropTypes from 'prop-types';

class Option extends React.Component {
    render() {
        return (
            <option
                disabled={this.props.disabled}
                value={this.props.value}
            >{this.props.text}</option>
        )
    }
}
Option.defaultProps = {
    disabled: false
}

Option.propTypes = {
    disabled: PropTypes.bool,
    text: PropTypes.any.isRequired
}

export default Option;