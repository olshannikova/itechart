import React from 'react';
import PropTypes from 'prop-types';

class Checkbox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isChecked: props.checked
        }
    }
    valueChangedHandler() {
        this.setState({isChecked: !this.state.isChecked});
    }
    render() {
        return (
            <input 
                type="checkbox"
                id={this.props.id}
                onChange={this.valueChangedHandler.bind(this)} 
                checked={this.state.checked}/>
        )
    }
}

Checkbox.defaultProps = {
    checked: false
}

Checkbox.propTypes = {
    checked: PropTypes.bool
}

export default Checkbox;