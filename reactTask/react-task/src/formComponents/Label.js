import React from 'react';
import PropTypes from 'prop-types';

class Label extends React.Component {
    render() {
        return (
            <label 
                className="react__Label"
                htmlFor={this.props.for}>
                {this.props.text}
            </label>
        );
    }
}

Label.propTypes = {
    for: PropTypes.any.isRequired
};

export default Label;