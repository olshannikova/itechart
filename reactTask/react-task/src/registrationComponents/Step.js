import React from 'react'
import PropTypes from 'prop-types'
import './css/Common.css'

class Step extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isFilled: {},
            available: this.props.available
        }
        this.localStorageValues = {};
    }
    componentWillReceiveProps(nextProps) {
        const newAvailable = nextProps["available"];
        if(newAvailable !== this.state.available) {
            this.setState({available: newAvailable}, this.isNextStepAvailable);
        }
    }
    componentWillMount() {
        if (localStorage.length !== 0) {
            const isFilled = this.state.isFilled;
            let newIsFilled = {};
            for (let key in isFilled) {
                const value = localStorage.getItem(key);
                if (value) {
                    this.localStorageValues[key] = value;
                    newIsFilled[key] = 1;
                }
            }
            newIsFilled = Object.assign({}, this.state.isFilled, newIsFilled);
            this.setState({isFilled: newIsFilled}, this.isNextStepAvailable);
        }
    }
    isInputFilled(id) {
        if (this.state.isFilled[id] === 0) {
            return false;
        }
        return true;
    }
    renderError(message, inputId) {
        return !this.isInputFilled(inputId) && <p className="error">{message}</p>;
    }
    onBlurHandler(id) {
        if (this.state.isFilled[id] === -1) {
            let newIsFilled = {};
            newIsFilled[id] = 0;
            newIsFilled = Object.assign({}, this.state.isFilled, newIsFilled);
            this.setState({isFilled: newIsFilled});
        }
    }
    makeStepAvailable(index) {
        if (this.state.available) {
            this.props.changeAvailable(index, true);
        }
    }
    makeStepNotAvailable(index) {
        this.props.changeAvailable(index, false);
    }
    render() {
        return(
            <section></section>
        );
    }
}

Step.propTypes = {
    changeAvailable: PropTypes.func
}

export default Step;