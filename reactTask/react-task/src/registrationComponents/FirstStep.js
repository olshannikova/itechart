import React from 'react'
import Input from '../formComponents/Input.js'
import Label from '../formComponents/Label.js'
import Step from './Step.js'
import './css/Common.css'

class FirstStep extends Step {
    constructor(props) {
        super(props);
        this.state = {
            isFilled: {
                "lastName": -1, 
                "firstName": -1, 
                "middleName": -1, 
                "age": -1, 
                "birthDate": -1
            },
            available: this.props.available
        }
        this.inputValueChanged = this.inputValueChanged.bind(this);
        this.isNextStepAvailable = this.isNextStepAvailable.bind(this);
    }
    isNextStepAvailable() {
        const isFilled = this.state.isFilled;
        for (let key in isFilled) {
            if (isFilled[key] !== 1) {
                    this.makeStepNotAvailable(1);
                    return;
                }
        }
        this.makeStepAvailable(1);
    }
    inputValueChanged(id, value) {
        let newIsFilled = {};
        if (value === "" || value === undefined) {
            newIsFilled[id] = 0;
        } else {
            newIsFilled[id] = 1;
        }
        newIsFilled = Object.assign({}, this.state.isFilled, newIsFilled);
        this.setState({isFilled: newIsFilled}, this.isNextStepAvailable);
        localStorage.setItem(id, value);
    }
    render() {
        const message = "Please, fill in this field";
        return(
            <section>
                <Label for="lastName" text="Last name" /> <br />
                <Input 
                    id="lastName" required={true} getValue={this.inputValueChanged} 
                    onBlur={this.onBlurHandler.bind(this, "lastName")} value={this.localStorageValues["lastName"]} 
                />
                {this.renderError(message, "lastName")}
                <br />

                <Label for="firstName" text="First name" /><br />
                <Input 
                    id="firstName" required={true} getValue={this.inputValueChanged} 
                    onBlur={this.onBlurHandler.bind(this, "firstName")} value={this.localStorageValues["firstName"]} 
                />
                {this.renderError(message, "firstName")}
                <br />

                <Label for="middleName" text="Middle name" /><br />
                <Input 
                    id="middleName" required={true} getValue={this.inputValueChanged} 
                    onBlur={this.onBlurHandler.bind(this, "middleName")} value={this.localStorageValues["middleName"]} 
                />
                {this.renderError(message, "middleName")}
                <br />

                <Label for="age" text="Age" /> <br/>
                <Input 
                    type="number" id="age" required={true} getValue={this.inputValueChanged} 
                    onBlur={this.onBlurHandler.bind(this, "age")} value={this.localStorageValues["age"]} 
                />
                {this.renderError(message, "age")}
                <br />

                <Label for="birthDate" text="Birth date" /><br />
                <Input 
                    type="date" id="birthDate" required={true} getValue={this.inputValueChanged} 
                    onBlur={this.onBlurHandler.bind(this, "birthDate")} value={this.localStorageValues["birthDate"]} 
                />
                {this.renderError(message, "birthDate")}
            </section>
        );
    }
}

export default FirstStep;