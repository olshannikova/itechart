import React from 'react'
import Input from '../formComponents/Input.js'
import Label from '../formComponents/Label.js'
import Step from './Step.js'
import './css/Common.css'

class SecondStep extends Step {
    constructor(props) {
        super(props);
        this.state = {
            isFilled: {
                "email": -1, 
                "phone": -1, 
                "website": -1, 
            },
            available: this.props.available
        }
        this.inputValueChanged = this.inputValueChanged.bind(this);
        this.isNextStepAvailable = this.isNextStepAvailable.bind(this);
    }
    isNextStepAvailable() {
        const isFilled = this.state.isFilled;
        for (let key in isFilled) {
            if (isFilled[key] !== 1) {
                    this.makeStepNotAvailable(2);
                    return;
                }
        }
        this.makeStepAvailable(2);
    }
    inputValueChanged(id, value) {
        let newIsFilled = {};
        if (value === "" || value === undefined) {
            newIsFilled[id] = 0;
        } else {
            newIsFilled[id] = 1;
        }
        newIsFilled = Object.assign({}, this.state.isFilled, newIsFilled);
        this.setState({isFilled: newIsFilled}, this.isNextStepAvailable);
        localStorage.setItem(id, value);
    }
    render() {
        const message = "Please, fill in this field";
        return(
            <section>
                <Label for="email" text="Email" /> <br />
                <Input id="email" required={true} getValue={this.inputValueChanged} onBlur={this.onBlurHandler.bind(this, "email")} value={this.localStorageValues["email"]} />
                {this.renderError(message, "email")}
                <br />

                <Label for="phone" text="Phone" /><br />
                <Input id="phone" required={true} getValue={this.inputValueChanged} onBlur={this.onBlurHandler.bind(this, "phone")} value={this.localStorageValues["phone"]} />
                {this.renderError(message, "phone")}
                <br />

                <Label for="website" text="Website" /><br />
                <Input id="website" required={true} getValue={this.inputValueChanged} onBlur={this.onBlurHandler.bind(this, "website")} value={this.localStorageValues["website"]} />
                {this.renderError(message, "website")}
                <br />
            </section>
        );
    }
}

export default SecondStep;