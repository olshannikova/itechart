import React from 'react'
import './css/InfoPopup.css'

class InfoPopup extends React.Component {
    confirmButtonClicked() {
        const ids = ["lastName", "firstName", "middleName", "age", "birthDate", "email", "phone", "website", "nickname"];
        for (let i = 0; i < ids.length; i += 1) {
            localStorage.removeItem(ids[i]);
        }
        this.props.onConfirmButtonClicked();
    }
    render() {
        return(
            <div className={"InfoPopup__wrap " + (this.props.hidden ? "InfoPopup__wrap-hidden" : "")}>
                <div className="InfoPopup__popup">
                    <table>
                        <thead></thead>
                        <tbody>
                            <tr><td>Last Name: </td><td>{localStorage.getItem("lastName")}</td></tr>
                            <tr><td>First Name: </td><td>{localStorage.getItem("firstName")}</td></tr>
                            <tr><td>Middle Name: </td><td>{localStorage.getItem("middleName")}</td></tr>
                            <tr><td>Age: </td><td>{localStorage.getItem("age")}</td></tr>
                            <tr><td>Birth date: </td><td>{localStorage.getItem("birthDate")}</td></tr>
                            <tr><td>Email: </td><td>{localStorage.getItem("email")}</td></tr>
                            <tr><td>Phone: </td><td>{localStorage.getItem("phone")}</td></tr>
                            <tr><td>Website: </td><td>{localStorage.getItem("website")}</td></tr>
                            <tr><td>Nickname: </td><td>{localStorage.getItem("nickname")}</td></tr>
                        </tbody>
                    </table>
                    <button onClick={this.confirmButtonClicked.bind(this)}>Confirm</button>
                </div>
            </div>
        )
    }
}


export default InfoPopup;