import React from 'react'
import PropTypes from 'prop-types'
import TabView from '../tabViewComponent/TabView.js'
import FirstStep from './FirstStep.js'
import SecondStep from './SecondStep.js'
import ThirdStep from './ThirdStep.js'
import FourthStep from './FourthStep.js'
import InfoPopup from './InfoPopup.js'

import './css/Registration.css'

let history = window.history;
let location = window.location;

class Registration extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            available: [true, false, false, false],
            currentTab: 0,
            popupHidden: true
        }
        this.changeAvailable = this.changeAvailable.bind(this);
    }
    componentWillMount() {
        const address = location.href;
        let step = address.substr(address.lastIndexOf("?") + 1);
        step = parseInt(step, 10);
        if (!Number.isNaN(step)) {
            if (step > this.state.available.length) {
                this.setState({currentTab: 0});
            } else {
                this.setState({currentTab: step});
            }
        }
    }
    pushHistory(tabIndex) {
        history.pushState(history.state, "", "?" + tabIndex);
    }
    changeAvailable(index, value) {
        let newAvailable = this.state.available;
        newAvailable[index] = value;
        if (!value) {
            for (let i = index + 1, length = this.state.available.length; i < length; i += 1) {
                newAvailable[i] = false;
            }
        }
        this.setState({available: newAvailable});
    }
    onSubmitButtonClick() {
        this.setState({popupHidden: false});
    }
    onConfirmButtonClicked() {
        this.setState({popupHidden: true});
        const address = location.href;
        location.href = address.substr(0, address.lastIndexOf("?"));
    }
    render() {
        return(
            <div>
            <InfoPopup hidden={this.state.popupHidden} onConfirmButtonClicked={this.onConfirmButtonClicked.bind(this)}/>
                <div className="Registration">
                    <TabView
                        defaultTab={this.state.currentTab}
                        currentTabChanged={this.pushHistory.bind(this)} 
                        tabsAmount={4}
                        tabNames={["step 1", "step 2", "step 3", "step 4"]}
                        tabContent={[
                            <FirstStep 
                                changeAvailable={this.changeAvailable}
                                available={this.state.available[0]}
                            />, 
                            <SecondStep 
                                changeAvailable={this.changeAvailable}
                                available={this.state.available[1]}
                            />, 
                            <ThirdStep 
                                changeAvailable={this.changeAvailable}
                                available={this.state.available[2]}
                            />, 
                            <FourthStep 
                                changeAvailable={this.changeAvailable}
                                available={this.state.available[3]}
                                onSubmitButtonClick={this.onSubmitButtonClick.bind(this)}
                            />
                        ]}
                        available={this.state.available}
                    />
                </div>
            </div>
        );
    }
}

export default Registration;