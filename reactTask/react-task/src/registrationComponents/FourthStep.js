import React from 'react'
import Input from '../formComponents/Input.js'
import Label from '../formComponents/Label.js'
import Step from './Step.js'
import './css/Common.css'

class FourthStep extends Step {
    constructor(props) {
        super(props);
        this.state = {
            isFilled: {
                "password": -1, 
                "confirmPassword": -1, 
            },
            passwordValues: {
                "password": "", 
                "confirmPassword": "", 
            },
            submitButtonDisabled: true,
            available: this.props.available
        }
        this.inputValueChanged = this.inputValueChanged.bind(this);
        this.isNextStepAvailable = this.isNextStepAvailable.bind(this);
        this.passwordsMatch = this.passwordsMatch.bind(this);
    }
    passwordsMatch() {
        const password = this.state.passwordValues["password"];
        const confirmPassword = this.state.passwordValues["confirmPassword"];
        if (password !== confirmPassword || password === confirmPassword === undefined) {
            return false;
        }
        return true;
    }
    isNextStepAvailable() {
        if (!this.passwordsMatch() || this.state.isFilled["password"] !== 1 || this.state.isFilled["confirmPassword"] !== 1) {
            this.setState({submitButtonDisabled: true});
            return;
        }
        this.setState({submitButtonDisabled: false});
    }
    inputValueChanged(id, value) {
        let newIsFilled = {};
        let newPasswordValues = {};
        if (value === "" || value === undefined) {
            newIsFilled[id] = 0;
        } else {
            newIsFilled[id] = 1;
        }
        newPasswordValues[id] = value;
        newPasswordValues = Object.assign({}, this.state.passwordValues, newPasswordValues);
        newIsFilled = Object.assign({}, this.state.isFilled, newIsFilled);
        this.setState({isFilled: newIsFilled, passwordValues: newPasswordValues}, this.isNextStepAvailable);
    }
    renderError(message, inputId) {
        return (!this.isInputFilled(inputId) || !this.passwordsMatch()) && <p className="error">{message}</p>;
    }
    sumbitButtonClicked() {
        this.props.onSubmitButtonClick();
    }
    render() {
        const notFilled = "Please, fill in this field";
        const dontMatch = "Passwords don't match";
        return(
            <section>
                <Label for="password" text="Password" /> <br />
                <Input id="password" type="password" required={true} getValue={this.inputValueChanged} onBlur={this.onBlurHandler.bind(this, "password")} />
                {this.renderError(this.state.isFilled["password"] ? dontMatch : notFilled, "password")}
                <br />

                <Label for="confirmPassword" text="Confirm password" /><br />
                <Input id="confirmPassword" type="password" required={true} getValue={this.inputValueChanged} onBlur={this.onBlurHandler.bind(this, "confirmPassword")} />
                {this.renderError(this.state.isFilled["confirmPassword"] ? dontMatch : notFilled, "confirmPassword")}
                <br />

                <button
                    disabled={this.state.submitButtonDisabled}
                    onClick={this.sumbitButtonClicked.bind(this)}
                >submit</button>
            </section>
        );
    }
}

export default FourthStep;