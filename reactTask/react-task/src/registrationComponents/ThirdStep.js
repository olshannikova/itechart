import React from 'react'
import Input from '../formComponents/Input.js'
import Label from '../formComponents/Label.js'
import Step from './Step.js'
import './css/Common.css'

class ThirdStep extends Step {
    constructor(props) {
        super(props);
        this.state = {
            isFilled: {
                "nickname": -1, 
            },
            available: this.props.available
        }
        this.inputValueChanged = this.inputValueChanged.bind(this);
        this.isNextStepAvailable = this.isNextStepAvailable.bind(this);
    }
    isNextStepAvailable() {
        if (this.state.isFilled["nickname"] !== 1) {
            this.makeStepNotAvailable(3);
            return;
        }
        this.makeStepAvailable(3);
    }
    inputValueChanged(id, value) {
        let newIsFilled = {};
        if (value === "" || value === undefined) {
            newIsFilled[id] = 0;
        } else {
            newIsFilled[id] = 1;
        }
        newIsFilled = Object.assign({}, this.state.isFilled, newIsFilled);
        this.setState({isFilled: newIsFilled}, this.isNextStepAvailable);
        localStorage.setItem(id, value);
    }
    render() {
        const message = "Please, fill in this field";
        return(
            <section>
                <Label for="nickname" text="Nickname" /> <br />
                <Input id="nickname" required={true} getValue={this.inputValueChanged} onBlur={this.onBlurHandler.bind(this, "nickname")} value={this.localStorageValues["nickname"]} />
                {this.renderError(message, "nickname")}
                <br />

                <Label for="avatar" text="Avatar" /><br />
                <Input id="avatar" type="file" accept="image/jpeg,image/png,image/gif" />
                {this.renderError(message, "avatar")}
                <br />
            </section>
        );
    }
}

export default ThirdStep;