import React from 'react'
import PropTypes from 'prop-types'
import Tab from './Tab.js'
import TabContent from './TabContent.js'
import './css/styles.css'

class TabView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentTab: this.props.defaultTab
        }
        this.changeCurrentTabCallback = this.changeCurrentTabCallback.bind(this);
    }
    changeCurrentTabCallback() {
        if (this.props.currentTabChanged) {
            this.props.currentTabChanged(this.state.currentTab);
        }
    }
    changeCurrentTab(value) {
        this.setState({currentTab: value}, this.changeCurrentTabCallback);
    }
    render() {
        let tabs = [];
        let tabContent = [];
        for (let i = 0; i < this.props.tabsAmount; i += 1) {
            tabs.push(<Tab 
                        key={"tab-header-" + i} 
                        tabName={this.props.tabNames[i]} 
                        index={i} 
                        currentTab={this.state.currentTab} 
                        changeCurrentTab={this.changeCurrentTab.bind(this)}
                        available={this.props.available[i]} 
                    />);
            tabContent.push(<TabContent 
                                key={"tab-content" + i} 
                                index={i} 
                                currentTab={this.state.currentTab}
                                >
                                    {this.props.tabContent[i]}
                            </TabContent>);
        }
        return(
            <div>
                <ul className="tab-headers">{tabs}</ul>
                {tabContent}
            </div>
        );
    }
}

TabView.propTypes = {
    tabsAmount: PropTypes.number,
    tabNames: PropTypes.array,
    tabContent: PropTypes.array
}

export default TabView;