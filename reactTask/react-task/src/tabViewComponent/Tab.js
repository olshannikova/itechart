import React from 'react'
import PropTypes from 'prop-types'

class Tab extends React.Component {
    onClickHandler() {
        if (this.props.available) {
            this.props.changeCurrentTab(this.props.index);
        }
    }
    render() {
        return(
            <li 
                onClick={this.onClickHandler.bind(this)}
                className={"tab " + (this.props.currentTab === this.props.index ? "selected-tab " : "") + (this.props.available ? "" : "not-available-tab")}
            >
                {this.props.tabName}
            </li>
        );
    }
}

Tab.propTypes = {
    index: PropTypes.number.isRequired,
    currentTab: PropTypes.number.isRequired,
    changeCurrentTab: PropTypes.func.isRequired,
    tabName: PropTypes.any.isRequired,
    available: PropTypes.bool.isRequired
}

export default Tab;