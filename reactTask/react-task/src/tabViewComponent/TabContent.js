import React from 'react'
import PropTypes from 'prop-types'
import './css/styles.css'

class TabContent extends React.Component {
    render() {
        return(
            <div className={this.props.index === this.props.currentTab ? "selected-content" : "hidden-content"}>
                {this.props.children}
            </div>
        );
    }
}

TabContent.propTypes = {
    index: PropTypes.number.isRequired,
    currentTab: PropTypes.number.isRequired,
    children: PropTypes.any
}

export default TabContent;