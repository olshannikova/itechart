;(function($) {
    var methods = {
        addItems: function(elements) { 
            $(this).append(elements);
            return this;
        },
        refresh: function(elements) {
            $(this).empty().append(elements);
            return this;
        },
        clean: function() {
            $(this).empty();
            return this;
        }
    }
    $.fn.myPlugin = function(method) {
        return methods[method].apply(this, [].slice.call(arguments, 1));
    }
})(jQuery);