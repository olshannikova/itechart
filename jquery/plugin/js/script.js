(function() {
    function getSelectedItems() {
        var checkboxes = $("input[type=checkbox]");
        var items, i, id, label;
        items = [];
        for (i = 0; i < checkboxes.length; i += 1) {
            if ($(checkboxes[i]).is(":checked")) {
                id = $(checkboxes[i]).attr("id");
                items.push($("label[for=" + id + "]").text());   
            }
        }
        return items;
    }
    
    $("#addItems").on("click",  function() {
        var elements = getSelectedItems();
        var i;
        for (i = 0; i < elements.length; i += 1) {
            elements[i] = "<" + elements[i] + ">" + "this is " + elements[i] + "</" + elements[i] + ">";
        }
        $("#myDiv").myPlugin("addItems", elements);
    });
    
    $("#refresh").on("click",  function() {
        var elements = getSelectedItems();
        var i;
        for (i = 0; i < elements.length; i += 1) {
            elements[i] = "<" + elements[i] + ">" + "this is " + elements[i] + "</" + elements[i] + ">";
        }
        $("#myDiv").myPlugin("refresh", elements);
    });
    
    $("#clean").on("click",  function() {
        $("#myDiv").myPlugin("clean");
    });
    
    $("#loadJSON").on("click", function() {
        $.ajax({
            type: 'GET',
            url: '/js/list.json',
            dataType: 'json',
            success: function(data) {
                var elements = [];
                $.each(data, function(i, item) {
                    elements.push("<" + item.item + ">" + item.content + "<" + item.item + "/>");
                })
                $("#myDiv").myPlugin("refresh", elements);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert( "jqXHR.status: " + jqXHR.status + ", textStatus: " + textStatus + ", errorThrown: " + errorThrown);
            }
        })
    })
})();