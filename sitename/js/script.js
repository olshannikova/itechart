var asideButton = document.getElementById("aside-button");
asideButton.onclick = function () {
    var asidePanel = document.getElementById("aside-panel");
    var mainCotainer = document.getElementById("main-container");
    if (asidePanel.style.overflow == "hidden")
        {
            asidePanel.style.overflow = "visible";
            mainCotainer.style.height = '0';
            mainCotainer.style.overflow = "hidden";
            asideButton.innerHTML = "hide";
        }
    else {
        asidePanel.style.overflow = "hidden";
        mainCotainer.style.overflow = "visible";
        asideButton.innerHTML = "menu";
    }
}