import React, { Component } from 'react';
import Calendar from './calendarComponents/Calendar.js'

class App extends Component {
  render() {
    return (
      <Calendar />
    );
  }
}

export default App;
