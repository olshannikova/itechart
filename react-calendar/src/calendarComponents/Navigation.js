import React from 'react';
import './css/Navigation.css'
import './css/common.css'

class Navigation extends React.Component {
  constructor(props) {
    super(props);
    this.type = props.type;
    this.goForward = this.goForward.bind(this);
    this.goBack = this.goBack.bind(this);
  }
  goForward() {
    const currentMonth = this.props.displayMonth;
    const currentYear = this.props.displayYear;
    if (currentMonth === 11) {
      this.props.changeYear(currentYear + 1);
      this.props.changeMonth(0);
    } else {
      this.props.changeMonth(currentMonth + 1);
    }
  }
  goBack() {
    const currentMonth = this.props.displayMonth;
    const currentYear = this.props.displayYear;
    if (currentMonth === 0) {
      this.props.changeYear(currentYear - 1);
      this.props.changeMonth(11);
    } else {
      this.props.changeMonth(currentMonth - 1);
    }
  }
  onClickHandler() {
    if (this.type === "forward") {
      this.goForward();
    } else if (this.type === "back") {
      this.goBack();
    }
  }
  render() {
    let arrow = "";
    if (this.props.type === "forward") {
      arrow = "›";
    } else if (this.props.type === "back") {
      arrow = "‹";
    }
    return (
      <span className="CalendarNavigation noselect" onClick={this.onClickHandler.bind(this)}> {arrow} </span>
    );
  }
}

export default Navigation;
