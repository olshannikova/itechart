import React from 'react';
import './css/Day.css'
import './css/common.css'

class Day extends React.Component {
    constructor() {
        super();
        this.monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    }
    onClick() {
        if (this.props.isActive) {
            const week = this.props.week;
            this.props.onDayClick({day: this.props.day, week: week.props.weekNum + 1, month: week.props.displayMonth, year: week.props.displayYear});
        }
    }
  render() {
    return (
        <td onClick={this.onClick.bind(this)}>
            <div className={"CalendarDay_wrap " + (this.props.isCurrentDay ? "CalendarDay_wrap-currentDay" : " ") + (this.props.isActive ? " " : "CalendarDay_wrap-notActive")}>
                <span 
                    className={"CalendarDay noselect " + (this.props.isActive ? " " : "CalendarDay-notActive ") + (this.props.isWeekend ? "CalendarDay-weekend" : " ") }
                >{this.props.day}</span>
            </div>
        </td>
    );
  }
}

export default Day;
