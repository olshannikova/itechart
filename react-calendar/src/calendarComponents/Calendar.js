import React, { Component } from 'react';
import Header from './Header.js'
import Week from './Week.js'
import './css/Calendar.css'

class Calendar extends Component {
  constructor() {
    super();
    this.monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    this.state = {
      displayMonth: new Date().getMonth(),
      displayYear: new Date().getFullYear(),
      popupHidden: true,
      dayInfo: {
        day: 0,
        week: 0,
        month: 0,
        year: 0
      }
    }
    this.changeMonth = this.changeMonth.bind(this);
    this.changeYear = this.changeYear.bind(this);
    this.getNumOfWeeks = this.getNumOfWeeks.bind(this);
    this.renderPopUp = this.renderPopUp.bind(this);
  }
  changeMonth(month) {
    this.setState({displayMonth: month});
  }
  changeYear(year) {
    this.setState({displayYear: year});
  }
  getNumOfWeeks() {
    const lastMonthDay = new Date(this.state.displayYear, this.state.displayMonth + 1, 0);
    const numOfDays = lastMonthDay.getDate();
    const weekDay = lastMonthDay.getDay();
    const prevLastMonthWeekDay = new Date(this.state.displayYear, this.state.displayMonth, 0).getDay();
    if (prevLastMonthWeekDay !== 6) {
      return (numOfDays - (weekDay + 1) - (6 - prevLastMonthWeekDay)) / 7 + 2;
    } else {
      return (numOfDays - (weekDay + 1) - (6 - prevLastMonthWeekDay)) / 7 + 1;
    }
  }
  renderPopUp() {
    let week;
    switch(this.state.dayInfo.week) {
      case 1:
        week = "1st week";
        break;
      case 2:
        week = "2nd week";
        break;
      case 3:
        week = "3rd week";
        break;
      default:
        week = this.state.dayInfo.week + "th week";
        break;
    }
    return (
      <div className={"CalendarPopup__wrap " + (this.state.popupHidden ? "CalendarPopup__wrap-hidden" : "")}>
        <div className="CalendarPopup__popup">
          <button onClick={this.hidePopup.bind(this)}>x</button>
            <h1>{this.monthNames[this.state.dayInfo.month] + " " + this.state.dayInfo.day + ", " + this.state.dayInfo.year}</h1>
            <h2>{week}</h2>
          </div>
      </div>
    );
  }
  hidePopup() {
    this.setState({popupHidden: true});
  }
  onDayClick(dayInfo) {
    this.setState({popupHidden: false, dayInfo: dayInfo});
  }
  render() {
    const numOfWeeks = this.getNumOfWeeks();
    let weeks = [];
    for (let i = 0; i < numOfWeeks; i += 1) {
      weeks.push(<Week key={"CalendarWeek-" + i} displayMonth={this.state.displayMonth} displayYear={this.state.displayYear} weekNum={i} onDayClick={this.onDayClick.bind(this)}/>);
    }
    return (
      <div>
        {this.renderPopUp()}
        <div className="Calendar">
          <Header displayMonth={this.state.displayMonth} displayYear={this.state.displayYear} changeMonth={this.changeMonth} changeYear={this.changeYear} />
          <table>
            <tbody>
            <tr><th>SUN</th><th>MON</th><th>TUE</th><th>WED</th><th>THU</th><th>FRI</th><th>SAT</th></tr>
            {weeks}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default Calendar;
