import React from 'react';
import './css/Title.css'
import './css/common.css'

class Title extends React.Component {
  render() {
    return (
      <div className="CalendarTitle_wrap"><h1 className="CalendarTitle noselect">{this.props.month}  {this.props.year}</h1></div>
    );
  }
}

export default Title;
