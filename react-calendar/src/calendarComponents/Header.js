import React, { Component } from 'react';
import Title from './Title.js'
import Navigation from './Navigation.js'

class Header extends Component {
  constructor() {
    super();
    this.monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", "Months"];
  }
  render() {
    return (
      <div>
        <Navigation type="back" 
          changeMonth={this.props.changeMonth} 
          changeYear={this.props.changeYear} 
          displayMonth={this.props.displayMonth}
          displayYear={this.props.displayYear}
        />
        <Title month={this.monthNames[this.props.displayMonth]} year={this.props.displayYear}/>
        <Navigation type="forward" 
          changeMonth={this.props.changeMonth} 
          changeYear={this.props.changeYear}
          displayMonth={this.props.displayMonth}
          displayYear={this.props.displayYear} 
        />
      </div>
    );
  }
}

export default Header;
