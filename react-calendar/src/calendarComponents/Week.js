import React from 'react';
import Day from './Day.js'

class Week extends React.Component {
  render() {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth();
    const currentDay = currentDate.getDate();

    let days = [];
    const displayMonth = this.props.displayMonth;
    const displayYear = this.props.displayYear;
    const firstWeekDay = new Date(displayYear, displayMonth, 1).getDay();

    const weekNum = this.props.weekNum;
    const lastWeekDay = 7 - firstWeekDay + weekNum * 7;

    const lastMonthDay = new Date(displayYear, displayMonth + 1, 0).getDate();
    const prevLastMonthDay = new Date(displayYear, displayMonth, 0).getDate();

    const start = lastWeekDay - 6;
    for (let i = start; i <= lastWeekDay; i += 1) {
        let day = i;
        let isActive = true;
        if (day <= 0) {
            day = prevLastMonthDay + day;
            isActive = false;
        } else if (day > lastMonthDay) {
            day = day - lastMonthDay;
            isActive = false;
        }
        days.push(<Day 
                    key={"CalendarDay-" + day}
                    day={day} 
                    isActive={isActive} 
                    isCurrentDay={(currentDay === day) && (currentYear === displayYear) && (currentMonth === displayMonth) ? true : false} 
                    isWeekend={isActive && (day === lastWeekDay || day === (start)) ? true : false}
                    week={this}
                    onDayClick={this.props.onDayClick}
                />)
    }
    return (
      <tr>{days}</tr>
    );
  }
}

export default Week;
