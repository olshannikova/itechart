function WizardViewModel(numberOfSteps, types, ids, labelNames) {
    var self = this;
    self.numberOfSteps = numberOfSteps;
    self.steps = [];
    self.currentStep = ko.observable(0);
    self.popupHidden = ko.observable(true);

    for (var i = 0; i < self.numberOfSteps; i += 1) {
        self.steps.push(new StepViewModel(i, types[i], ids[i], labelNames[i]));
    }
    self.isCurrentStep = function(step) {
        return step === self.currentStep();
    };
    /*
    * Checks if all sections between two steps are filled
    * {return} not filled section's index or -1 if everything is filled
    */
    self.allStepsFilled = function(from, to) {
        var i, j, step;
        for (i = from; i < to; i += 1) {
            step = self.steps[i];
            for (j = 0; j < step.fields.length; j += 1) {
                if (step.fields[j].isFilled() !== 1) {
                    return i;
                }
            }
        }
        return -1;
    }
    /*
    * This function can be used only in goToStep function
    * Checks if current step is filled. If it's not, function marks not filled input
    */ 
    function isCurrentStepFilled(step) {
        var notFilledInput = self.steps[self.currentStep()].allFilled();
        if (notFilledInput !== -1 && step > self.currentStep()) {
            self.steps[self.currentStep()].fields[notFilledInput].isFilled(0);
            return false;
        }
        return true;
    }
    /*
    * This function can be used only in goToStep function
    * Checks if all steps between current and destination are filled. If it's not
    * function redirects user to not filled step
    */ 
    function isSomeStepFilled(step) {
        var notFilledStep = self.allStepsFilled(self.currentStep(), step);
        if (notFilledStep !== -1) {
            self.steps[self.currentStep()].saveToLocalStorage();
            self.currentStep(notFilledStep);
            history.pushState(history.state, "", "?" + notFilledStep);
            return false;
        }
        return true;
    }
    self.goToStep = function(step) {
        if (!isCurrentStepFilled(step) || !isSomeStepFilled(step)) {
            return;
        }
        self.steps[self.currentStep()].saveToLocalStorage();
        self.currentStep(step);
        history.pushState(history.state, "", "?" + self.currentStep());
    }
    self.goBack = function() {
        if (self.canGoBack()) {
            self.steps[self.currentStep()].saveToLocalStorage();
            self.currentStep(self.currentStep() - 1);
            history.pushState(history.state, "", "?" + self.currentStep());
        }
    }
    self.goForward = function() {
        if (self.canGoForward()) {
            var notFilled = self.steps[self.currentStep()].allFilled();
            if(notFilled !== -1) {
                self.steps[self.currentStep()].fields[notFilled].isFilled(0);
                return;
            }
            self.steps[self.currentStep()].saveToLocalStorage();
            self.currentStep(self.currentStep() + 1);
            history.pushState(history.state, "", "?" + self.currentStep());
        }
    }
    self.canGoBack = function() {
        if (self.currentStep() === 0) {
            return false;
        }
        return true;
    }
    self.canGoForward = function() {
        if (self.currentStep() === self.numberOfSteps - 1) {
            return false;
        }
        return true;
    }
    /*
    * Fills all inputs with values from localStorage
    */
    self.getStorageInfo = function() {
        var i, j, val, step;
        for (i = 0; i < self.steps.length; i += 1) {
            step = self.steps[i];
            for (j = 0; j < step.fields.length; j += 1) {
                val = localStorage.getItem(step.fields[j].labelName);
                if (val !== null) {
                    step.fields[j].value(val);
                    step.fields[j].isFilled(1);
                }
            }
        }
    }
    self.passwordsMatch = function() {
        if ($("#pass").val() !== $("#confirmPass").val() || $("#pass").val() === $("#confirmPass").val() === undefined) {
            return false;
        }
        return true;
    }
    self.fillPopup = function() {
        var value, table, html, key, i, j, step;
        $("#popup").find("h2").after("<table></table>");
        table = $("#popup").find("table");
        for (i = 0; i < self.steps.length; i += 1) {
            step = self.steps[i];
            for (j = 0; j < step.fields.length; j += 1) {
                key = step.fields[j].labelName;
                value = localStorage.getItem(key);
                if (value !== null) {
                    html = "<tr><td>" + key + "</td><td>" + value + "</td></tr>";
                    table.append(html);
                }
            }
        }
    }
    self.confirmForm = function() {
        var notFilled = self.steps[self.currentStep()].allFilled();
        if (notFilled !== -1) {
            self.steps[self.currentStep()].fields[notFilled].isFilled(0);
            return;
        }
        if(!self.passwordsMatch()) {
            $("#confirmPass").addClass("not-filled");
            return;
        }
        self.fillPopup();
        self.popupHidden(false);
        console.log(self.popupHidden());
    }
    window.onload = function() {
        if (localStorage.length !== 0) {
            self.getStorageInfo();
        }
        var address = location.href;
        var step = address.substr(address.lastIndexOf("?") + 1);
        step = parseInt(step, 10);
        if (!Number.isNaN(step)) {
            if (step > self.numberOfSteps) {
                self.goToStep(0);
            } else {
                self.goToStep(step);
            }
        }
    }
    self.clearLocalStorage = function() {
        var i, j, val, step;
        for (i = 0; i < self.steps.length; i += 1) {
            step = self.steps[i];
            for (j = 0; j < step.fields.length; j += 1) {
                localStorage.removeItem(step.fields[j].labelName);
            }
        }
    }
    self.sendForm = function() {
        $("form").submit();
        self.clearLocalStorage();
        self.popupHidden(true);
    }
}