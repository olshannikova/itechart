/*
* View model representing single required input
*/
function FieldViewModel(type, id, labelName) {
    var self = this;
    self.isFilled = ko.observable(-1);
    self.value = ko.observable("");
    self.type = type;
    self.id = id;
    self.labelName = labelName;

    self.focusOut = function() {
        if (self.value() === "") {
            self.isFilled(0);
        } else {
            self.isFilled(1);
        }
    }
    self.focusIn = function() {
        self.isFilled(-1);
    }
}