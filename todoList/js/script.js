(function(){
    var noteTitle = document.getElementById("note-title"),
        noteDetails = document.getElementById("note-details"),
        noteId = document.getElementById("noteId"),
        editNote = document.getElementById("edit-note"),
        saveNote = document.getElementById("save-note"),
        saveChanges = document.getElementById("save-changes"),
        notes = document.getElementById("notes"),
        closeNote = document.getElementById("close-note"),
        addNote = document.getElementById("add-note"),
        popup = document.getElementById("popup");

    window.onload = function() {
        var address = location.href;
        var id = address.substr(address.lastIndexOf("?") + 1);
        id = parseInt(id, 10);
        if (!isNaN(id)) {
            id = id.toString();
            var notes = document.getElementsByClassName("show-popup");
            for (var i = 0; i < notes.length; i++) {
                if(notes[i].dataset.noteId == id) {
                        notes[i].click();
                        break;
                    }
            }
        }
    }
    
    if (!localStorage.getItem('noteId')) {
        localStorage.setItem('noteId', 0);
    }
    
    notes.onclick = function(e) {
        if (e.target.className == "show-popup") {
            var id = e.target.dataset.noteId;
            var note = localStorage.getItem(id);
            note = JSON.parse(note);
            noteTitle.value = note.title;
            noteDetails.value = note.details;
            editNote.style.display = "inline";
            popup.style.display = "block";
            noteId.value = id;
            history.pushState(history.state, "", "?" + id);
        } else if (e.target.className == "delete-note") {
            localStorage.removeItem(e.target.dataset.noteId);
            location.reload();
        }
    }

    closeNote.onclick = function() {
        popup.style.display = "none";
        editNote.style.display = "none";
        saveNote.style.display = "none";
        saveChanges.style.display = "none";
        noteTitle.readOnly = true;
        noteTitle.value = '';
        noteDetails.readOnly = true;
        noteDetails.value = '';
        history.pushState(history.state, "", "index.html");
    }

    addNote.onclick = function() {
        noteId.value = '';
        popup.style.display = "block";
        saveNote.style.display = "inline";
        noteTitle.readOnly = false;
        noteDetails.readOnly = false;
    }

    editNote.onclick = function() {
        noteTitle.readOnly = false;
        noteDetails.readOnly = false;
        noteTitle.style.cursor = "text";
        noteDetails.style.cursor = "text";
        editNote.style.display = "none";
        saveChanges.style.display = "inline";
    }

    saveChanges.onclick = function() {
        var id = noteId.value;
        var note = {
            title: noteTitle.value,
            details: noteDetails.value
        };
        localStorage.setItem(id, JSON.stringify(note));
        saveChanges.style.display = "none";
        editNote.style.display = "inline";
        noteTitle.readOnly = true;
        noteDetails.readOnly = true;
        noteTitle.style.cursor = "default";
        noteDetails.style.cursor = "default";
    }

    saveNote.onclick = function() {
        var id = parseInt(localStorage.getItem('noteId'), 10);
        var note = {
            title: noteTitle.value,
            details: noteDetails.value
        };
        localStorage.setItem(id, JSON.stringify(note));
        localStorage.setItem('noteId', id + 1);
    }

    var notesStr = "";
    for (var i = 0; i < localStorage.length; i++) {
        var notesList = document.getElementById("notes-list");
        var key = localStorage.key(i);
        if (key != "noteId") {
            var note = localStorage.getItem(key);
            note = JSON.parse(note);
            notesStr += "<div data-node-id=\"" + key + "\"><li>" + note.title + "</li><button data-note-id=\"" + key + "\" class=\"show-popup\">details</button> <button data-note-id=\"" + key + "\" class=\"delete-note\">delete</button></div>";
        }
        notesList.innerHTML = notesStr;
    }
})();